<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 09.08.14
 * Time: 16:23
 */

namespace Soglasie\CommonBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class TelType extends AbstractType
{
    /**
     * @return  string
     */
    public function getName()
    {
        return 'tel';
    }

    /**
     * @return  string
     */
    public function getParent()
    {
        return 'text';
    }
}