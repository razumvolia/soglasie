$(function(){
    $('input[placeholder], textarea[placeholder]').placeholder();
    $("input[type=tel]").mask("+7(999)999-99-99");

    var note = $('#note'),
            ts = new Date(2014,7,31),
            newYear = true;

    if((new Date()) > ts){
        // The new year is here! Count towards something else.
        // Notice the *1000 at the end - time must be in milliseconds
        ts = (new Date()).getTime() + 10*24*60*60*1000;
        newYear = false;
    }

    $('#countdown').countdown({
        timestamp	: ts,
        callback	: function(days, hours, minutes, seconds){

            var message = "";

            message += days + " day" + ( days==1 ? '':'s' ) + ", ";
            message += hours + " hour" + ( hours==1 ? '':'s' ) + ", ";
            message += minutes + " minute" + ( minutes==1 ? '':'s' ) + " and ";
            message += seconds + " second" + ( seconds==1 ? '':'s' ) + " <br />";

            if(newYear){
                message += "left until the new year!";
            }
            else {
                message += "left to 10 days from now!";
            }

            note.html(message);
        }
    });

});

$(document).mouseup(function(){
    var checkbox1 = "";

    if( $('.footerBlock input#check-1').is(':checked') ) {
        checkbox1 = $('input#check-1:checked').val();
    }

    var checkbox2 = "";

    if( $('.footerBlock input#check-2').is(':checked') ) {
        checkbox2 = $('input#check-2:checked').val();
    }

    var checkbox3 = "";

    if( $('.footerBlock input#check-3').is(':checked') ) {
        checkbox3 = $('input#check-3:checked').val();
    }

    var checkbox4 = "";

    if( $('.footerBlock input#check-4').is(':checked') ) {
        checkbox4 = $('input#check-4:checked').val();
    }

    $('#form3 input.box1').val(checkbox1);
    $('#form3 input.box2').val(checkbox2);
    $('#form3 input.box3').val(checkbox3);
    $('#form3 input.box4').val(checkbox4);

    return false;
});
