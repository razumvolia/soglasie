<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 24.09.14
 * Time: 16:58
 */

namespace Soglasie\CommonBundle\Controller;

use Soglasie\CommonBundle\Entity\Order;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AdminController extends Controller
{
    const FILE_EXT = '.jpg';

    public function editAction($id = null)
    {
        $response = parent::editAction($id);

        if (null !== $this->get('request')->get('btn_nbki')) {

            $nbkiManager = $this->get('nbki_manager');
            /** @var $object \Soglasie\CommonBundle\Entity\Order */
            $object = $this->admin->getObject($id);

            if (!$object) {
                throw new NotFoundHttpException(sprintf('sonata.error.object_not_found ', $id));
            }

            $orderData = $object->getOrderData();
            $nbkiResponse = $nbkiManager->sendRequest([
                'firstName' => $orderData->getFirstName(),
                'secondName' => $orderData->getMiddleName(),
                'lastName' => $orderData->getLastName(),
                'placeOfBirth' => 'Москва',
                'birthDate' => $orderData->getBirthDate(),
                'idSeries' => $orderData->getPassportSeries(),
                'idNumber' => $orderData->getPassportNumber()
            ]);

            $filename = $this->get('kernel')->getRootDir() . DIRECTORY_SEPARATOR . $this->container->getParameter('uploads_dir') .
                DIRECTORY_SEPARATOR . $id . self::FILE_EXT;
            @unlink($filename);

            $this->get('knp_snappy.image')->generateFromHtml(
                $this->renderView(
                    'SoglasieCommonBundle:Admin:pdf.html.twig',
                    array(
                        'nbkiResponse'  => $nbkiResponse
                    )
                ),
                $filename
            );

            $object->setStatus(Order::STATUS_SENT);
            $this->admin->update($object);

            $this->addFlash('sonata_flash_success', 'sonata.nbki.success');
        }

        return $response;
    }

    public function dataAction() {
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $filename = $id . self::FILE_EXT;

        return $this->get('igorw_file_serve.response_factory')->create($filename, 'image/jpeg');
    }
}
