<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:43
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class TotalMoney {
	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("Code")
	 * */
	protected $code;

	/**
	 * @JMS\Type("integer")
	 * @JMS\SerializedName("Value")
	 * */
	protected $value;

	function getCode() {
		return $this->code;
	}

	function getValue() {
		return $this->value;
	}
}
