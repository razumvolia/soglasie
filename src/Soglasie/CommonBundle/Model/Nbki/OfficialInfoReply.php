<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:42
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class OfficialInfoReply {
	/**
	 * @JMS\Type("integer")
	 * */
	protected $serialNum;

	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("Source")
	 * */
	protected $source;

	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("Information")
	 * */
	protected $information;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * */
	protected $dateReported;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $freezeFlag;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $suppressFlag;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $disputedStatus;

	/**
	 * @JMS\Type("string")
	 * */
	protected $disputedRemarks;

	function getSerialNum() {
		return $this->serialNum;
	}

	function getSource() {
		return $this->source;
	}

	function getInformation() {
		return $this->information;
	}

	function getDateReported() {
		return $this->dateReported;
	}

	function getFreezeFlag() {
		return $this->freezeFlag;
	}

	function getSuppressFlag() {
		return $this->suppressFlag;
	}

	function getDisputedStatus() {
		return $this->disputedStatus;
	}

	function getDisputedRemarks() {
		return $this->disputedRemarks;
	}
}
