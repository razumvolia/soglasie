<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:31
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class PersonReq {
	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("name1")
	 * */
	protected $lastName;

	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("first")
	 * */
	protected $firstName;

	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("paternal")
	 * */
	protected $secondName;

	/**
	 * @JMS\Type("string")
	 * */
	protected $gender = 1;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("birthDt")
	 * */
	protected $birthDate;

	/**
	 * @JMS\Type("string")
	 * */
	protected $placeOfBirth;

	function getLastName() {
		return $this->lastName;
	}

	function setLastName( $lastName ) {
		$this->lastName = $lastName;
	}

	function getFirstName() {
		return $this->firstName;
	}

	function setFirstName( $firstName ) {
		$this->firstName = $firstName;
	}

	function getSecondName() {
		return $this->secondName;
	}

	function setSecondName( $secondName ) {
		$this->secondName = $secondName;
	}

	function getGender() {
		return $this->gender;
	}

	function getBirthDate() {
		return $this->birthDate;
	}

	function setBirthDate( $birthDate ) {
		$this->birthDate = $birthDate;
	}

	function getPlaceOfBirth() {
		return $this->placeOfBirth;
	}

	function setPlaceOfBirth( $placeOfBirth ) {
		$this->placeOfBirth = $placeOfBirth;
	}

}
