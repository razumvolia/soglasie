<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:32
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation\Type;

class InquiryReq {
	/**
	 * @Type("integer")
	 * */
	protected $inqPurpose = 50;

	/**
	 * @Type("integer")
	 * */
	protected $inqAmount = 0;

	/**
	 * @Type("string")
	 * */
	protected $currencyCode = 'RUB';

}
