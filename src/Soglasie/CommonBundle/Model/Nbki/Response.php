<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 04.08.14
 * Time: 16:11
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;
use Soglasie\CommonBundle\Model\Nbki\PReply;

class Response extends Request {
	/**
	 * @JMS\SerializedName("preply")
	 * @JMS\Type("Soglasie\CommonBundle\Model\Nbki\PReply")
	 * */
	protected $pReply;

	function __construct() {
		parent::__construct();
	}

	/**
	 * @return PReply
	 */
	function getPReply() {
		return $this->pReply;
	}
}
