<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:39
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class SubjectReply {
	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("lastUpdatedDt")
	 * */
	protected $lastUpdatedDate;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("fileSinceDt")
	 * */
	protected $fileSinceDate;

	function getLastUpdateDate() {
		return $this->lastUpdatedDate;
	}

	function getFileSinceDate() {
		return $this->fileSinceDate;
	}
}
