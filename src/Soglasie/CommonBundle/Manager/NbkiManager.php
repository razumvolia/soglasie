<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 28.07.14
 * Time: 15:30
 */

namespace Soglasie\CommonBundle\Manager;

use Buzz\Browser;
use Soglasie\CommonBundle\Model\Nbki\Request;
use Soglasie\CommonBundle\Model\Nbki\Response;
use JMS\Serializer\Serializer;

class NbkiManager
{
    /**
     * @var $nbkiRequest Request
     * @var $serializer Serializer
     * @var $nbkiResponse Response
     * @var $browser Browser
     */
    private
        $server, $nbkiRequest, $serializer, $browser;

    function __construct($server, $memberCode, $userId, $password, Browser $browser, Serializer $serializer)
    {
        $this->serializer = $serializer;
        $this->server = $server;
        $this->browser = $browser;
        $this->browser->getClient()->setVerifyPeer(false);

	    $this->nbkiRequest = new Request();
        $this->nbkiRequest
            ->setMemberCode($memberCode)
            ->setUserId($userId)
            ->setPassword($password);
    }

    function sendRequest($values)
    {
        $this->nbkiRequest->setFirstName($values['firstName']);
        $this->nbkiRequest->setSecondName($values['secondName']);
        $this->nbkiRequest->setLastName($values['lastName']);

        $this->nbkiRequest->setPlaceOfBirth($values['placeOfBirth']);
        $this->nbkiRequest->setBirthDate($values['birthDate']);

        $this->nbkiRequest->setIdNumber($values['idNumber']);
        $this->nbkiRequest->setIdSeries($values['idSeries']);

        $request = $this->serializer->serialize($this->nbkiRequest, 'xml');
        $response = $this->browser->post($this->server, array('Content-Type: text/plain'), $request)->getContent();

        preg_match("~<\?xml.+</product>~sm", $response, $matches);
        $xmlResponse = $matches[0];

        /** @var $nbkiResponse Response */
        $nbkiResponse = $this->serializer->deserialize($xmlResponse, 'Soglasie\CommonBundle\Model\Nbki\Response', 'xml');

        /** @var $calc \Soglasie\CommonBundle\Model\Nbki\Calc */
        $calc = $nbkiResponse->getPReply()->getReport()->getCalc();

        /** @var $loans \Soglasie\CommonBundle\Model\Nbki\AccountReply[] */
        $loans = $nbkiResponse->getPReply()->getReport()->getAccountReplies();

        foreach ($loans as $loan) {
            $calc->setTotal30Days($calc->getTotal30Days() + $loan->getNumDays30());
            $calc->setTotal60Days($calc->getTotal60Days() + $loan->getNumDays60());
            $calc->setTotal90Days($calc->getTotal90Days() + $loan->getNumDays90());
        }

        return $nbkiResponse;
    }

}
