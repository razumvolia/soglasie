<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 08.08.14
 * Time: 17:17
 */

namespace Soglasie\CommonBundle\Controller;

use Doctrine\ORM\EntityManager;
use JMS\DiExtraBundle\Annotation as DI;
use JMS\Payment\CoreBundle\Entity\ExtendedData;
use JMS\Payment\CoreBundle\Entity\Payment;
use JMS\Payment\CoreBundle\Entity\PaymentInstruction;
use JMS\Payment\CoreBundle\Form\ChoosePaymentMethodType;
use JMS\Payment\CoreBundle\Plugin\Exception\Action\VisitUrl;
use JMS\Payment\CoreBundle\Plugin\Exception\ActionRequiredException;
use JMS\Payment\CoreBundle\PluginController\PluginController;
use JMS\Payment\CoreBundle\PluginController\Result;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Soglasie\CommonBundle\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;

class PaymentController extends Controller
{
    /** @var $router Router */
    private $router;

    /** @var $em EntityManager */
    private $em;

    /**
     * @var $ppc PluginController
     * @DI\Inject("payment.plugin_controller")
     */
    private $ppc;

    /**
     * @Template
     * @param Order $order
     * @return array | RedirectResponse
     */
    public function detailsAction(Order $order)
    {
        // Create the extended data object
        $extendedData = new ExtendedData();

        // Complete payment return URL
        $extendedData->set(
            'return_url',
            $this->router->generate(
                'payment_complete',
                array(
                    'id' => $order->getId(),
                ),
                true
            )
        );

        // Cancel payment return URL
        $extendedData->set(
            'cancel_url',
            $this->router->generate(
                'payment_cancel',
                array(
                    'id' => $order->getId(),
                ),
                true
            )
        );

        // Create the payment instruction object
        $instruction = new PaymentInstruction(
            $order->getAmount(), 'RUR', 'robokassa', $extendedData
        );

        // Validate and persist the payment instruction
        $this->get('payment.plugin_controller')->createPaymentInstruction($instruction);

        // Update the order object
        $order->setPaymentInstruction($instruction);
        $this->em->flush();

        // Continue with payment
        return new RedirectResponse(
            $this->router->generate(
                'payment_complete',
                array(
                    'id' => $order->getId(),
                )
            )
        );

        /*        $form = $this->getFormFactory()->create(
                    'jms_choose_payment_method',
                    null,
                    array(
                        'amount' => $order->getAmount(),
                        'currency' => 'RUR',
                        'default_method' => 'robokassa',
                        'allowed_methods' => array('robokassa'),
                        'predefined_data' => array(
                            'robokassa' => array(
                                'return_url' => $this->router->generate(
                                    'payment_complete',
                                    array(
                                        'id' => $order->getId(),
                                    ),
                                    true
                                ),
                                'cancel_url' => $this->router->generate(
                                    'payment_cancel',
                                    array(
                                        'id' => $order->getId(),
                                    ),
                                    true
                                )
                            ),
                            'qiwi_wallet' => array(
                                'comment' => 'Покупка услуги Согласие-Финанс, заявка №' .
                                    $order->getId() . ' от ' . $order->getUser()->getName(),
                                'lifetime' => (new \DateTime())->modify("+2 days"),
                                'alarm' => true,
                                'create' => true,
                                'return_url' => $this->router->generate('payment_complete', ['id' => $order->getId()]), true),
                                'cancel_url' => $this->router->generate('payment_cancel', ['id' => $order->getId()], true)
                        )
                    )
                );

                if ('POST' === $this->request->getMethod()) {
                    $form->handleRequest($this->request);

                    if ($form->isValid()) {
                        $this->ppc->createPaymentInstruction($instruction = $form->getData());

                        $order->setPaymentInstruction($instruction);
                        $this->em->persist($order);
                        $this->em->flush($order);

                        return new RedirectResponse(
                            $this->router->generate('payment_complete', ['id' => $order->getId()])
                        );
                    }
                }

                return array(
                    'form' => $form->createView()
                );*/
    }

    /**
     * @Template
     *
     * @throws ActionRequiredException
     * @var Order $order
     * @return array
     * */
    public function completeAction(Order $order)
    {
        if (Order::STATUS_PAID == $order->getStatus() || Order::STATUS_SENT == $order->getStatus()) {
            $this->get('session')->getFlashBag()->add(
                'error',
                'Ошибка! Некорректный статус заявки с номером ' . $order->getId() . '. Обратитесь в нашу службу поддержки по e-mail 23@согласие-финанс.рф'
            );
            return array();
        }
        $senderEmail = $this->container->getParameter('sender');
        $senderName = $this->container->getParameter('sender_name');

        $instruction = $order->getPaymentInstruction();
        if (null === $pendingTransaction = $instruction->getPendingTransaction()) {
            $payment = $this->ppc->createPayment(
                $instruction->getId(),
                $instruction->getAmount() - $instruction->getDepositedAmount()
            );
        } else {
            $payment = $pendingTransaction->getPayment();
        }

        $result = $this->ppc->approveAndDeposit($payment->getId(), $payment->getTargetAmount());
        if (Result::STATUS_PENDING === $result->getStatus()) {
            $ex = $result->getPluginException();

            if ($ex instanceof ActionRequiredException) {
                $action = $ex->getAction();

                if ($action instanceof VisitUrl) {
                    return new RedirectResponse($action->getUrl());
                }

                throw $ex;
            }
        } else {
            if (Result::STATUS_SUCCESS !== $result->getStatus()) {
                throw new \RuntimeException('Платёжная транзакция не прошла, код ошибки: ' . $result->getReasonCode());
            }
        }

        if (0 === $order->getAmount()) {
            return new Response($this->renderView('SoglasieCommonBundle:Email:order_paid_to_user.txt.twig'));
        }

        $order->setStatus(Order::STATUS_PAID);

        $subject = "Оплачена заявка #" . $order->getId();
        $email = $order->getUser()->getEmail();
        if ($email) {
            $orderPaidMessageToUser = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setTo($email)
                ->setFrom($senderEmail , $senderName)
                ->setReplyTo($senderEmail , $senderName)
                ->setSender($senderEmail , $senderName)
                ->setReturnPath($senderEmail)
                ->setBody(
                    $this->renderView(
                        'SoglasieCommonBundle:Email:order_paid_to_user.txt.twig',
                        array(
                            'order' => $order
                        )
                    )
                )
            ;
            $this->get('mailer')->send($orderPaidMessageToUser);
        }
        $orderPaidMessage = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setTo('dima@glavcredit.com')
            ->setFrom($senderEmail, $senderName)
            ->setReplyTo($senderEmail, $senderName)
            ->setSender($senderEmail, $senderName)
            ->setReturnPath($senderEmail)
            ->setBody(
                $this->renderView(
                    'SoglasieCommonBundle:Email:order_paid.txt.twig',
                    array(
                        'order' => $order
                    )
                )
            )
        ;
        $this->get('mailer')->send($orderPaidMessage);

        $this->em->flush();

        return array(
            'order' => $order
        );
    }

    /**
     * @Template
     * @var Order $order
     * @return array
     * */
    public function cancelAction(Order $order)
    {
        return array('order' => $order);
    }

    /**
     * @return FormFactory
     * @DI\LookupMethod("form.factory")
     */
    protected function getFormFactory()
    {
    }
}
