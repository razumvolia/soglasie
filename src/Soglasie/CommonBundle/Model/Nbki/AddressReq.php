<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:32
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation\Type;

class AddressReq {
	const
		ADDRESS_TYPE_REGISTRATION = 1,
		ADDRESS_TYPE_RESIDENTIAL = 2;

	/**
	 * @Type("string")
	 * */
	protected $street = 'Красная';

	/**
	 * @Type("integer")
	 * */
	protected $houseNumber = 0;

	/**
	 * @Type("integer")
	 * */
	protected $apartment = 0;

	/**
	 * @Type("string")
	 * */
	protected $city = 'Москва';

	/**
	 * @Type("integer")
	 * */
	protected $postal = 0;

	/**
	 * @Type("integer")
	 * */
	protected $addressType = 1;

	function setAddressType( $addressType ) {
		$this->addressType = $addressType;
	}
}
