<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 03.08.14
 * Time: 18:22
 */

namespace Soglasie\CommonBundle\Tests\NbkiManager;

use Soglasie\CommonBundle\Manager\NbkiManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class NbkiManagerTest extends WebTestCase
{
    static $browser, $serializer, $container;

    static public function setUpBeforeClass()
    {
        //start the symfony kernel
        $kernel = static::createKernel();
        $kernel->boot();

        //get the DI container
        self::$container = $kernel->getContainer();

        //now we can instantiate our service (if you want a fresh one for
        //each test method, do this in setUp() instead
        self::$browser = self::$container->get('buzz');
        self::$browser->getClient()->setTimeout(20);
        self::$serializer = self::$container->get('serializer');
    }

    function testNbkiManager()
    {
        $server = 'http://icrs.demo.nbki.ru/products/B2BRequestServlet';
        $memberCode = 'KH01FF000000';
        $userId = 'KH01FF000005';
        $password = 'LLZYtb73';

        $manager = new NbkiManager($server, $memberCode, $userId, $password, self::$browser, self::$serializer);
        $response = $manager->sendRequest([
            'firstName' => 'Георгий',
            'secondName' => 'Георгиевич',
            'lastName' => 'Кустов',
            'placeOfBirth' => 'Москва',
            'birthDate' => new \DateTime('1946-07-06'),
            'idNumber' => '123456',
            'idSeries' => '1234'
        ]);

        /** @var \Soglasie\CommonBundle\Model\Nbki\Report $report */
        $report = $response->getPReply()->getReport();
        $this->assertEquals(count($report->getIdReplies()), 5);
        $this->assertEquals(count($report->getAddressReplies()), 6);
    }
}
