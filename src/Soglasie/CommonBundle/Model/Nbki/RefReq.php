<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:30
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation\Type;

class RefReq {
	/**
	 * @Type("string")
	 * */
	protected $product = 'CHST';
}
