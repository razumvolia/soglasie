<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 26.09.14
 * Time: 16:11
 */

namespace Soglasie\CommonBundle\Manager;

use Buzz\Browser;

class SmsManager
{
    private $browser, $login, $password, $charset;

    function __construct(Browser $browser, $login, $password, $charset)
    {
        $this->browser = $browser;
        $this->login = $login;
        $this->password = $password;
        $this->charset = $charset;
    }

    function send($phone, $message)
    {
        $url = 'http://smsc.ru/sys/send.php?login=' . $this->login .
            '&psw=' . $this->password .
            '&phones=' . $phone .
            '&charset=' . $this->charset .
            '&mes=' . urlencode($message);
        $this->browser->get($url);
    }
}
