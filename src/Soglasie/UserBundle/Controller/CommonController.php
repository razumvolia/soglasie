<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 08.07.14
 * Time: 9:35
 */

namespace Soglasie\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class CommonController extends Controller
{
    /**
     * @Template()
     * */
    public function indexAction()
    {
        return array();
    }
}
