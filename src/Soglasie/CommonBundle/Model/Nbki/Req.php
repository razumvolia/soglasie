<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:32
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class Req {
	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("IOType")
	 * */
	protected $ioType = 'B2B';

	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("OutputFormat")
	 * */
	protected $outputFormat = 'XML';

	/**
	 * @JMS\Type("string")
	 * */
	protected $lang = 'ru';

	/**
	 * @JMS\SerializedName("RefReq")
	 * @JMS\Type("Soglasie\CommonBundle\Model\Nbki\RefReq")
	 * */
	protected $refReq;

	/**
	 * @JMS\SerializedName("RequestorReq")
	 * @JMS\Type("Soglasie\CommonBundle\Model\Nbki\RequestorReq")
	 * */
	protected $requestorReq;

	/**
	 * @JMS\SerializedName("PersonReq")
	 * @JMS\Type("Soglasie\CommonBundle\Model\Nbki\PersonReq")
	 * */
	protected $personReq;

	/**
	 * @JMS\SerializedName("InquiryReq")
	 * @JMS\Type("Soglasie\CommonBundle\Model\Nbki\InquiryReq")
	 * */
	protected $inquiryReq;

	/**
	 * @JMS\SerializedName("IdReq")
	 * @JMS\Type("Soglasie\CommonBundle\Model\Nbki\IdReq")
	 * */
	protected $idReq;

	/**
	 * @JMS\XmlList(inline = true, entry = "AddressReq")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\AddressReq>")
	 * */
	protected $addressReqs = array();

	function __construct() {
		$this->idReq        = new IdReq();
		$this->refReq       = new RefReq();
		$this->inquiryReq   = new InquiryReq();
		$this->personReq    = new PersonReq();
		$this->requestorReq = new RequestorReq();

		$addressReq = new AddressReq();
		$addressReq->setAddressType( AddressReq::ADDRESS_TYPE_RESIDENTIAL );
		$this->addressReqs = array( new AddressReq(), $addressReq );
	}

	function getIdReq() {
		return $this->idReq;
	}

	function getPersonReq() {
		return $this->personReq;
	}

	function getRequestorReq() {
		return $this->requestorReq;
	}

	function getAddressReqs() {
		return $this->addressReqs;
	}
}
