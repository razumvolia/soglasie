<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:40
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class AddressReply extends AddressReq {
	/**
	 * @JMS\Type("integer")
	 * */
	protected $serialNum;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("fileSinceDt")
	 * */
	protected $fileSinceDate;

	/**
	 * @JMS\Type("string")
	 * */
	protected $district;

	/**
	 * @JMS\Type("string")
	 * */
	protected $block;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $building;

	/**
	 * @JMS\Type("integer")
	 * @JMS\SerializedName("prov")
	 * */
	protected $province;

	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("provText")
	 * */
	protected $provinceText;

	/**
	 * @JMS\Type("string")
	 * */
	protected $countryCode;

	/**
	 * @JMS\Type("string")
	 * */
	protected $countryCodeText;

	/**
	 * @JMS\Type("string")
	 * */
	protected $addressTypeText;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("lastUpdatedDt")
	 * */
	protected $lastUpdatedDate;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $freezeFlag;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $suppressFlag;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $disputedStatus;

	/**
	 * @JMS\Type("string")
	 * */
	protected $disputedRemarks;

	function getSerialNum() {
		return $this->serialNum;
	}

	function getFileSinceDate() {
		return $this->fileSinceDate;
	}

	function getDistrict() {
		return $this->district;
	}

	function getBlock() {
		return $this->block;
	}

	function getBuilding() {
		return $this->building;
	}

	function getProvince() {
		return $this->province;
	}

	function getProvinceText() {
		return $this->provinceText;
	}

	function getCountryCode() {
		return $this->countryCode;
	}

	function getCountryCodeText() {
		return $this->countryCodeText;
	}

	function getAddressTypeText() {
		return $this->addressTypeText;
	}

	function getLastUpdatedDate() {
		return $this->lastUpdatedDate;
	}

	function getFreezeFlag() {
		return $this->freezeFlag;
	}

	function getSuppressFlag() {
		return $this->suppressFlag;
	}

	function getDisputedStatus() {
		return $this->disputedStatus;
	}

	function getDisputedRemarks() {
		return $this->disputedRemarks;
	}
}
