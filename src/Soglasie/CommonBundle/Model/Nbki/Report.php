<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:43
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class Report {
	/**
	 * @JMS\Type("Soglasie\CommonBundle\Model\Nbki\PersonReply")
	 * */
	protected $personReply;

	/**
	 * @JMS\XmlList(inline = true, entry = "IdReply")
	 * @JMS\SerializedName("IdReply")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\IdReply>")
	 * */
	protected $idReplies;

	/**
	 * @JMS\XmlList(inline = true, entry = "AddressReply")
	 * @JMS\SerializedName("AddressReply")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\AddressReply>")
	 * */
	protected $addressReplies;

	/**
	 * @JMS\XmlList(inline = true, entry = "PhoneReply")
	 * @JMS\SerializedName("PhoneReply")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\PhoneReply>")
	 * */
	protected $phoneReplies;

	/**
	 * @JMS\XmlList(inline = true, entry = "EmploymentReply")
	 * @JMS\SerializedName("EmploymentReply")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\EmploymentReply>")
	 * */
	protected $employmentReplies;

	/**
	 * @JMS\XmlList(inline = true, entry = "AccountReply")
	 * @JMS\SerializedName("AccountReply")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\AccountReply>")
	 * */
	protected $accountReplies;

	/**
	 * @JMS\XmlList(inline = true, entry = "LegalItemsReply")
	 * @JMS\SerializedName("LegalItemsReply")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\LegalItemsReply>")
	 * */
	protected $legalItemsReplies;

	/**
	 * @JMS\XmlList(inline = true, entry = "OfficialInfoReply")
	 * @JMS\SerializedName("OfficialInfoReply")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\OfficialInfoReply>")
	 * */
	protected $officialInfoReplies;

	/**
	 * @JMS\XmlList(inline = true, entry = "InquiryReply")
	 * @JMS\SerializedName("InquiryReply")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\InquiryReply>")
	 * */
	protected $inquiryReplies;

	/**
	 * @JMS\XmlList(inline = true, entry = "OwnAccount")
	 * @JMS\SerializedName("OwnAccount")
	 * @JMS\Type("array<Soglasie\CommonBundle\Model\Nbki\OwnAccount>")
	 * */
	protected $ownAccounts;

	/**
	 * @JMS\Type("Soglasie\CommonBundle\Model\Nbki\Calc")
	 * */
	protected $calc;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $inqControlNum;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $addOns;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $groups;

	/**
	 * @JMS\Type("integer")
	 * @JMS\SerializedName("integer")
	 * */
	protected $memberCodeStatus;

	function __construct() {
		$this->personReply = new PersonReply();
		$this->idReplies   = array( new IdReply() );
	}

	function getPersonReply() {
		return $this->personReply;
	}

	function getIdReplies() {
		return $this->idReplies;
	}

	function getAddressReplies() {
		return $this->addressReplies;
	}

	function getPhoneReplies() {
		return $this->phoneReplies;
	}

	function getEmploymentReplies() {
		return $this->employmentReplies;
	}

	function getAccountReplies() {
		return $this->accountReplies;
	}

	function getLegalItemsReplies() {
		return $this->legalItemsReplies;
	}

	function getOfficialInfoReplies() {
		return $this->officialInfoReplies;
	}

	function getInquiryReplies() {
		return $this->inquiryReplies;
	}

	function getOwnAccounts() {
		return $this->ownAccounts;
	}

	/**
	 * @return Calc
	 */
	function getCalc() {
		return $this->calc;
	}

	function getInqControlNum() {
		return $this->inqControlNum;
	}

	function getAddOns() {
		return $this->addOns;
	}

	function getGroups() {
		return $this->groups;
	}

	function getMemberCodeStatus() {
		return $this->memberCodeStatus;
	}
}
