<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:42
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class LegalItemsReply {
	/**
	 * @JMS\Type("integer")
	 * */
	protected $serialNum;

	/**
	 * @JMS\Type("string")
	 * */
	protected $filingNum;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("fileSinceDt")
	 * */
	protected $fileSinceDate;

	/**
	 * @JMS\Type("string")
	 * */
	protected $court;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("reportedDt")
	 * */
	protected $reportedDate;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("satisfiedDt")
	 * */
	protected $satisfiedDate;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("consideredDt")
	 * */
	protected $consideredDate;

	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("plaintiff")
	 * */
	protected $plainTiff;

	/**
	 * @JMS\Type("string")
	 * */
	protected $resolution;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("lastUpdateDt")
	 * */
	protected $lastUpdateDate;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $freezeFlag;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $suppressFlag;

	/**
	 * @JMS\Type("string")
	 * */
	protected $disputedStatus;

	/**
	 * @JMS\Type("string")
	 * */
	protected $disputedRemarks;

	function getSerialNum() {
		return $this->serialNum;
	}

	function getFilingNum() {
		return $this->filingNum;
	}

	function getFileSinceDate() {
		return $this->fileSinceDate;
	}

	function getCourt() {
		return $this->court;
	}

	function getReportedDate() {
		return $this->reportedDate;
	}

	function getSatisfiedDate() {
		return $this->satisfiedDate;
	}

	function getConsideredDate() {
		return $this->consideredDate;
	}

	function getPlainTiff() {
		return $this->plainTiff;
	}

	function getResolution() {
		return $this->resolution;
	}

	function getLastUpdateDate() {
		return $this->lastUpdateDate;
	}

	function getFreezeFlag() {
		return $this->freezeFlag;
	}

	function getSuppressFlag() {
		return $this->suppressFlag;
	}

	function getDisputedStatus() {
		return $this->disputedStatus;
	}

	function getDisputedRemarks() {
		return $this->disputedRemarks;
	}
}
