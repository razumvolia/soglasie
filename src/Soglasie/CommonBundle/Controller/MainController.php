<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 12.07.14
 * Time: 15:59
 */

namespace Soglasie\CommonBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Util\TokenGenerator;
use JMS\DiExtraBundle\Annotation as DI;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Soglasie\CommonBundle\Entity\Order;
use Soglasie\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Soglasie\CommonBundle\Form\Type\OrderType;

class MainController extends Controller
{
    /** @var EntityManager $em */
    private $em;

    /** @var Request $request */
    private $request;

    /**
     * @var $userManager UserManager
     * @DI\Inject("fos_user.user_manager")
     */
    private $userManager;

    /**
     * @var $tokenGenerator TokenGenerator
     * @DI\Inject("fos_user.util.token_generator")
     * */
    private $tokenGenerator;

    private function clearPhone($phone)
    {
        $phone = preg_replace("~\D+~", '', $phone);
        return $phone;
    }

    /**
     * @return array
     * @Template
     * */
    public function indexAction()
    {
        $geoDetectionManager = $this->get('geo_manager');
        $bitrix24Manager = $this->get('bitrix_manager');
        $smsManager = $this->get('sms_manager');

        $mailer = $this->get('mailer');

        $orderRepository = $this->getDoctrine()->getRepository('SoglasieCommonBundle:Order');
        $senderEmail = $this->container->getParameter('sender');
        $senderName = $this->container->getParameter('sender_name');

        $forms = array();

        for ($i = 0; $i < 7; $i++) {
            $formName = 'order_form_' . $i;
            $form = $this->createForm(new OrderType($formName, $this->generateUrl('main')));

            $form->handleRequest($this->request);

            if ($form->isValid()) {
                $name = $form->get('name')->getData();
                $phone = $form->get('phone')->getData();
                $email = $form->get('email')->getData();
                $amount = $form->get('amount')->getData();
                $username = $this->clearPhone($phone);
                $ipRegion = $geoDetectionManager->detectByIp($this->request->getClientIp());
                $phoneRegion = $geoDetectionManager->detectByPhone($phone);
                if (!empty($ipRegion) && !empty($phoneRegion)) {
                    $address = "$ipRegion/$phoneRegion";
                } else {
                    $address = "$ipRegion$phoneRegion";
                }

                /**
                 * @var $user User
                 * */
                $user = $this->userManager->findUserByUsername($username);

                if (!$user) {
                    $password = substr($this->tokenGenerator->generateToken(), 0, 8);
                    $user = $this->userManager->createUser();

                    // TODO: clean this
                    if ('dev' == $this->get('kernel')->getEnvironment()) {
                        $password = 111;
                    }
                    $user
                        ->setUsername($username)
                        ->setEmail($email)
                        ->setName($name)
                        ->setPlainPassword($password)
                        ->setEnabled(true)
                        ->setRoles(array('ROLE_USER'))
                        ->setPhone($username)
                    ;

                    $smsManager->send($username, $this->renderView(
                            'SoglasieCommonBundle:Email:welcome_sms.txt.twig',
                            array(
                                'name' => $name,
                                'username' => $username,
                                'password' => $password
                            )
                        ));

                    if ($email) {
                        $subject = "ВАША РЕГИСТРАЦИЯ НА САЙТЕ СОГЛАСИЕ-ФИНАНС.РФ";
                        $welcomeMessage = \Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($senderEmail, $senderName)
                            ->setReplyTo($senderEmail, $senderName)
                            ->setSender($senderEmail, $senderName)
                            ->setReturnPath($senderEmail)
                            ->setTo($email)
                            ->setBody(
                                $this->renderView(
                                    'SoglasieCommonBundle:Email:welcome.txt.twig',
                                    array(
                                        'name' => $name,
                                        'email' => $email,
                                        'phone' => $phone,
                                        'username' => $username,
                                        'password' => $password
                                    )
                                )
                            )
                        ;
                        $mailer->send($welcomeMessage);
                    }
                    $bitrixId = $bitrix24Manager->createLead($name, $phone, $email, $address);
                    $user->setBitrixId($bitrixId);
                } else {
                    $user
                        ->setEmail($email)
                        ->setName($name);
                }
                $this->userManager->updateUser($user);

                $order = $orderRepository->findOneBy(['user' => $user, 'status' => Order::STATUS_CREATED, 'type' => $i]);

                if (!$order) {
                    $order = new Order();

                    $order->setUser($user);
                    $order->setAmount($amount ? : 450);
                    $order->setStatus(Order::STATUS_CREATED);
                    $order->setType($i);

                    $this->em->persist($order);
                    $this->em->flush();

                    $subject = 'Пользователь ' . $order->getUser()->getName() . ' подал на сайте заявку #' . $order->getId();
                    $newOrderMessage = \Swift_Message::newInstance()
                         ->setSubject($subject)
                         ->setFrom($senderEmail, $senderName)
                         ->setReplyTo($senderEmail, $senderName)
                         ->setSender($senderEmail, $senderName)
                         ->setReturnPath($senderEmail)
                         // TODO: clean this
                         ->setTo('dima@glavcredit.com')
                         ->setBody(
                             $this->renderView(
                                 'SoglasieCommonBundle:Email:new_order.txt.twig',
                                 array(
                                     'order' => $order,
                                     'address' => $address,
                                 )
                             )
                         )
                    ;
                    $mailer->send($newOrderMessage);
                }

                $subjectToUser = $order->getUser()->getName() . ', ВЫ ОСТАВИЛИ ЗАЯВКУ №' . $order->getId() .
                    ' НА САЙТЕ HTTP://СОГЛАСИЕ-ФИНАНС.РФ/';

                if ($email) {
                    $newOrderMessageToUser = \Swift_Message::newInstance()
                        ->setSubject($subjectToUser)
                        ->setFrom($senderEmail, $senderName)
                        ->setReplyTo($senderEmail, $senderName)
                        ->setSender($senderEmail, $senderName)
                        ->setReturnPath($senderEmail)
                        ->setTo($email)
                        ->setBody(
                            $this->renderView(
                                'SoglasieCommonBundle:Email:new_order_to_user.txt.twig',
                                array(
                                    'order' => $order
                                )
                            )
                        )
                    ;
                    $mailer->send($newOrderMessageToUser);
                }

                return new Response($order->getId());
            }

            $forms[$formName] = $form->createView();
        }

        return $forms;
    }

    /**
     * @return array
     * @Template("")
     * */
    function bankrotstvoAction()
    {
        return $this->indexAction();
    }
}
