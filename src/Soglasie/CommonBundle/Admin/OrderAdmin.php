<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 31.08.14
 * Time: 20:08
 */

namespace Soglasie\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Soglasie\CommonBundle\Entity\Order;

class OrderAdmin extends Admin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->clearExcept(['edit', 'list'])
            ->add('data', $this->getRouterIdParameter() . '/data')
        ;
    }

    public function createQuery($context = 'list')
    {
        /* @var $securityToken \Symfony\Component\Security\Core\Authentication\Token\TokenInterface */
        $securityToken = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken();
        $user = $securityToken->getUser();

        /** @var $query \Doctrine\ORM\QueryBuilder */
        $query = parent::createQuery($context);
        $tableName = $query->getRootAliases()[0];
        $query
            ->andWhere(
                $query->expr()->eq($tableName . '.user', ':user')
            )
            ->setParameter('user', $user);
        return $query;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('orderData', 'sonata_type_admin', [
                'label' => false, 'delete' => false
            ])
            ->add('status', 'choice', array(
                'label' => 'Статус',
                'choices' => Order::getStatusesList(),
                'disabled' => true,
                'attr' => []
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id', null, ['label' => 'sonata.field.number'], 'number')
            ->add('status', null, [
                    'label' => 'sonata.field.status'
                ], 'choice',
                [
                    'choices' => Order::getStatusesList(),
                ]
            );
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, ['label' => 'sonata.field.number'])
            ->add('created', 'datetime', [
                'label' => 'sonata.field.created', 'template' => 'SoglasieCommonBundle:Admin:list__created.html.twig'
            ])
            ->add('status', 'text', [
                'label' => 'sonata.field.status', 'template' => 'SoglasieCommonBundle:Admin:list__status.html.twig'
            ])
            ->add('_action', 'actions', array(
                'label' => 'sonata.field.actions',
                    'label_catalogue' => 'SoglasieCommonBundle',
                'actions' => array(
                    'pay' => array('template' => 'SoglasieCommonBundle:Admin:list__action_pay.html.twig'),
                    'nbki' => array('template' => 'SoglasieCommonBundle:Admin:list__action_nbki.html.twig'),
                    'data' => array('template' => 'SoglasieCommonBundle:Admin:list__action_data.html.twig'),
                )
            ))
        ;
    }
}