<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:39
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class PersonReply extends PersonReq {
	/**
	 * @JMS\Type("integer")
	 * */
	protected $serialNum;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("fileSinceDt")
	 * */
	protected $fileSinceDate;

	/**
	 * @JMS\Type("string")
	 * */
	protected $genderText;

	/**
	 * @JMS\Type("string")
	 * */
	protected $nationality;

	/**
	 * @JMS\Type("string")
	 * */
	protected $nationalityText;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $maritalStatus;

	/**
	 * @JMS\Type("string")
	 * */
	protected $maritalStatusText;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $numDependants;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $deathFlag;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("lastUpdatedDt")
	 * */
	protected $lastUpdatedDate;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $freezeFlag;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $suppressFlag;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $disputedStatus;

	/**
	 * @JMS\Type("string")
	 * */
	protected $disputedRemarks;

	function getSerialNum() {
		return $this->serialNum;
	}

	function getFileSinceDate() {
		return $this->fileSinceDate;
	}

	function getGenderText() {
		return $this->genderText;
	}

	function getNationality() {
		return $this->nationality;
	}

	function getNationalityText() {
		return $this->nationalityText;
	}

	function getMaritalStatus() {
		return $this->maritalStatus;
	}

	function getMaritalStatusText() {
		return $this->maritalStatusText;
	}

	function getNumDependants() {
		return $this->numDependants;
	}

	function getDeathFlag() {
		return $this->deathFlag;
	}

	function getLastUpdatedDate() {
		return $this->lastUpdatedDate;
	}

	function getFreezeFlag() {
		return $this->freezeFlag;
	}

	function getSuppressFlag() {
		return $this->suppressFlag;
	}

	function getDisputedStatus() {
		return $this->disputedRemarks;
	}

	function getDisputedRemarks() {
		return $this->disputedRemarks;
	}
}
