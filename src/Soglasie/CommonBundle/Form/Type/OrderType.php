<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 28.08.14
 * Time: 9:30
 */

namespace Soglasie\CommonBundle\Form\Type;

use Soglasie\CommonBundle\Form\Type\TelType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderType extends AbstractType
{
    private $name, $action;

    public function __construct($formName = 'order_form', $action = null)
    {
        $this->name = $formName;
        $this->action = $action;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->action) {
            $builder->setAction($this->action);
        }
        $builder
            ->add(
                'name',
                'text',
                [
                    'attr' => ['placeholder' => 'ФИО:']
                ]
            )
            ->add(
                'email',
                'email',
                [
                    'required' => false,
                    'attr' => ['placeholder' => 'E-mail:']
                ]
            )
            ->add(
                'phone',
                new TelType(),
                [
                    'attr' => ['placeholder' => 'Номер телефона:']
                ]
            )
            ->add('amount', 'hidden')
            ->add('type', 'hidden')
            ->add(
                'submit',
                'submit',
                [
                    'label' => 'Заказать',
                    'attr' => ['data-type' => 'submit']
                ]
            )
            ->add(
                'submit1',
                'submit',
                [
                    'label' => 'Оставить заявку',
                    'attr' => ['data-type' => 'submit']
                ]
            )
            ->add(
                'submit2',
                'submit',
                [
                    'label' => 'получить информацию',
                    'attr' => ['data-type' => 'submit']
                ]
            );
    }

    public function setName($formName)
    {
        $this->name = $formName;
    }

    public function getName()
    {
        return $this->name;
    }
}