<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:41
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class AccountReply
{
    /**
     * @JMS\Type("integer")
     * */
    protected $serialNum;

    /**
     * @JMS\Type("integer")
     * */
    protected $ownerIndic;

    /**
     * @JMS\Type("string")
     * */
    protected $ownerIndicText;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * @JMS\SerializedName("fileSinceDt")
     * */
    protected $fileSinceDate;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * @JMS\SerializedName("openedDt")
     * */
    protected $openedDate;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * @JMS\SerializedName("lastPaymtDt")
     * */
    protected $lastPaymtDate;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * @JMS\SerializedName("closedDt")
     * */
    protected $closedDate;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * @JMS\SerializedName("reportingDt")
     * */
    protected $reportingDate;

    /**
     * @JMS\Type("integer")
     * */
    protected $acctType;

    /**
     * @JMS\Type("string")
     * */
    protected $acctTypeText;

    /**
     * @JMS\Type("integer")
     * */
    protected $collateralCode;

    /**
     * @JMS\Type("string")
     * */
    protected $collateral2Text;

    /**
     * @JMS\Type("string")
     * */
    protected $currencyCode;

    /**
     * @JMS\Type("integer")
     * */
    protected $creditLimit;

    /**
     * Выплачено по кредиту
     * @JMS\Type("integer")
     * */
    protected $curBalanceAmt;

    /**
     * Текущая просроченная задолженность по кредиту
     * @JMS\Type("integer")
     * */
    protected $amtPastDue;

    /**
     * @JMS\Type("integer")
     * */
    protected $termsFrequency;

    /**
     * @JMS\Type("integer")
     * */
    protected $termsAmt;

    /**
     * Текущая задолженность по кредиту
     * @JMS\Type("integer")
     * */
    protected $amtOutstanding;

    /**
     * @JMS\Type("integer")
     * */
    protected $monthsReviewed;

    /**
     * @JMS\Type("integer")
     * */
    protected $numDays30;

    /**
     * @JMS\Type("integer")
     * */
    protected $numDays60;

    /**
     * @JMS\Type("integer")
     * */
    protected $numDays90;

    /**
     * @JMS\Type("integer")
     * */
    protected $paymtPat;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * @JMS\SerializedName("paymtPatStartDt")
     * */
    protected $paymtPatStartDate;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * @JMS\SerializedName("lastUpdatedDt")
     * */
    protected $lastUpdatedDate;

    /**
     * @JMS\Type("boolean")
     * */
    protected $freezeFlag;

    /**
     * @JMS\Type("boolean")
     * */
    protected $suppressFlag;

    /**
     * @JMS\Type("string")
     * */
    protected $paymtFreqText;

    /**
     * @JMS\Type("integer")
     * */
    protected $accountRating;

    /**
     * Состояние кредита в виде текста - "Счёт закрыт", "Просрочен" и т. д.
     * @JMS\Type("string")
     * */
    protected $accountRatingText;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * */
    protected $accountRatingDate;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * */
    protected $paymentDueDate;

    /**
     * @JMS\Type("DateTime<'Y-m-dP'>")
     * */
    protected $interestPaymentDueDate;

    /**
     * @JMS\Type("integer")
     * */
    protected $interestPaymentFrequencyCode;

    /**
     * @JMS\Type("string")
     * */
    protected $interestPaymentFrequencyText;

    /**
     * @JMS\Type("integer")
     * */
    protected $disputedStatus;

    /**
     * @JMS\Type("string")
     * */
    protected $disputedRemarks;

    function getSerialNum()
    {
        return $this->serialNum;
    }

    function getFileSinceDate()
    {
        return $this->fileSinceDate;
    }

    function getOwnerIndic()
    {
        return $this->ownerIndic;
    }

    function getOwnerIndicText()
    {
        return $this->ownerIndicText;
    }

    function getOpenedDate()
    {
        return $this->openedDate;
    }

    function getLastPaymtDate()
    {
        return $this->lastPaymtDate;
    }

    function getClosedDate()
    {
        return $this->closedDate;
    }

    function getReportingDate()
    {
        return $this->reportingDate;
    }

    function getAcctType()
    {
        return $this->acctType;
    }

    function getAcctTypeText()
    {
        return $this->acctTypeText;
    }

    function getCollateralCode()
    {
        return $this->collateralCode;
    }

    function getCollateral2Text()
    {
        return $this->collateral2Text;
    }

    function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    function getCreditLimit()
    {
        return $this->creditLimit;
    }

    function getCurBalanceAmt()
    {
        return $this->curBalanceAmt;
    }

    function getAmtPastDue()
    {
        return $this->amtPastDue;
    }

    function getTermsFrequency()
    {
        return $this->termsFrequency;
    }

    function getTermsAmt()
    {
        return $this->termsAmt;
    }

    function getAmtOutstanding()
    {
        return $this->amtOutstanding;
    }

    function getMonthsReviewed()
    {
        return $this->monthsReviewed;
    }

    function getNumDays30()
    {
        return $this->numDays30;
    }

    function getNumDays60()
    {
        return $this->numDays60;
    }

    function getNumDays90()
    {
        return $this->numDays90;
    }

    function getPaymtPat()
    {
        return $this->paymtPat;
    }

    function getPaymtPatStartDate()
    {
        return $this->paymtPatStartDate;
    }

    function getLastUpdatedDate()
    {
        return $this->lastUpdatedDate;
    }

    function getFreezeFlag()
    {
        return $this->freezeFlag;
    }

    function getSuppressFlag()
    {
        return $this->suppressFlag;
    }

    function getPaymtFreqText()
    {
        return $this->paymtFreqText;
    }

    function getAccountRating()
    {
        return $this->accountRating;
    }

    function getAccountRatingText()
    {
        return $this->accountRatingText;
    }

    function getAccountRatingDate()
    {
        return $this->accountRatingDate;
    }

    function getPaymentDueDate()
    {
        return $this->paymentDueDate;
    }

    function getInterestPaymentDueDate()
    {
        return $this->interestPaymentDueDate;
    }

    function getInterestPaymentFrequencyCode()
    {
        return $this->interestPaymentFrequencyCode;
    }

    function getInterestPaymentFrequencyText()
    {
        return $this->interestPaymentFrequencyText;
    }

    function getDisputedStatus()
    {
        return $this->disputedStatus;
    }

    function getDisputedRemarks()
    {
        return $this->disputedRemarks;
    }
}
