<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:29
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation\Type;

class PRequest {
	/**
	 * @Type("Soglasie\CommonBundle\Model\Nbki\Req")
	 * */
	protected $req;

	function __construct() {
		$this->req = new Req();
	}

	function getReq() {
		return $this->req;
	}
}
