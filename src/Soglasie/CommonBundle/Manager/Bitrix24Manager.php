<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 22.09.14
 * Time: 16:21
 */

namespace Soglasie\CommonBundle\Manager;

use Buzz\Browser;
use JMS\Serializer\Serializer;

class Bitrix24Manager
{
    const
        BITRIX_OK = 201,
        LEADCREATEPATH = '/crm/configs/import/lead.php';

    private
        $browser, $serializer, $url, $login, $password, $responsible_id;

    function __construct(Browser $browser, Serializer $serializer, $server, $login, $password, $responsible_id)
    {
        /** @var $serializer Serializer */
        $this->serializer = $serializer;

        /* @var $browser \Buzz\Browser */
        $this->browser = $browser;

        $this->url = $server . self::LEADCREATEPATH;
        $this->login = $login;
        $this->password = $password;
        $this->responsible_id = $responsible_id;
    }

    function createLead($name, $phone, $email, $address)
    {
        $requestParameters = [
            'LOGIN' => $this->login,
            'PASSWORD' => $this->password,
            'TITLE' => $name,
            'PHONE_MOBILE' => $phone,
            'EMAIL_HOME' => $email,
            'ADDRESS' => $address,
            'STATUS_ID' => 'ASSIGNED',
            'SOURCE_ID' => 'WEB',
            'ASSIGNED_BY_ID' => $this->responsible_id
        ];

        $bitrixResponse = $this->browser->post($this->url, [], $requestParameters)->getContent();
        $json_in = array("{'", "'}", "':'", "','");
        $json_out = array('{"', '"}', '":"', '","');
        $bitrixResponse = str_replace($json_in, $json_out, $bitrixResponse);

        $jsonResponse = $this->serializer->deserialize($bitrixResponse, 'ArrayCollection', 'json');
        if ($jsonResponse['error'] != self::BITRIX_OK) {
            throw new \Exception('Не удалось корректно добавить лид в CRM Bitrix24, ответ сервера: ' . $bitrixResponse);
        }
        return $jsonResponse['ID'];
    }
}
