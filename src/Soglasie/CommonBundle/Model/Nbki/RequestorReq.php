<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:31
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class RequestorReq {
	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("MemberCode")
	 * */
	protected $memberCode;
	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("UserID")
	 * */
	protected $userId;
	/**
	 * @JMS\Type("string")
	 * @JMS\SerializedName("Password")
	 * */
	protected $password;

	function setMemberCode( $memberCode ) {
		$this->memberCode = $memberCode;
	}

	function setUserId( $userID ) {
		$this->userId = $userID;
	}

	function setPassword( $password ) {
		$this->password = $password;
	}
}
