<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:43
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation\Type;

class PReply {
	/**
	 * @Type("Soglasie\CommonBundle\Model\Nbki\Report")
	 * */
	protected $report;

	function __construct() {
		$this->report = new Report();
	}

	function getReport() {
		return $this->report;
	}
}
