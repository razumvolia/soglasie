<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 02.08.14
 * Time: 14:42
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\XmlRoot("product")
 */
class Request {
	/**
	 * @JMS\Type("Soglasie\CommonBundle\Model\Nbki\PRequest")
	 * */
	protected $prequest;

	function __construct() {
		$this->prequest = new PRequest();
	}

	function setMemberCode( $memberCode ) {
		$this->prequest->getReq()->getRequestorReq()->setMemberCode( $memberCode );

		return $this;
	}

	function setUserId( $userId ) {
		$this->prequest->getReq()->getRequestorReq()->setUserId( $userId );

		return $this;
	}

	function setPassword( $password ) {
		$this->prequest->getReq()->getRequestorReq()->setPassword( $password );

		return $this;
	}

	function setFirstName( $firstName ) {
		$this->prequest->getReq()->getPersonReq()->setFirstName( $firstName );
	}

	function setSecondName( $secondName ) {
		$this->prequest->getReq()->getPersonReq()->setSecondName( $secondName );
	}

	function setLastName( $firstName ) {
		$this->prequest->getReq()->getPersonReq()->setLastName( $firstName );
	}

	function setBirthDate( $birthDate ) {
		$this->prequest->getReq()->getPersonReq()->setBirthDate( $birthDate );
	}

	function setPlaceOfBirth( $placeOfBirth ) {
		$this->prequest->getReq()->getPersonReq()->setPlaceOfBirth( $placeOfBirth );
	}

	function setIdSeries( $idSeries ) {
		$this->prequest->getReq()->getIdReq()->setIdSeries( $idSeries );
	}

	function setIdNumber( $idNumber ) {
		$this->prequest->getReq()->getIdReq()->setIdNumber( $idNumber );
	}
}
