<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:41
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation\Type;

class InquiryReply {
	/**
	 * @Type("integer")
	 * */
	protected $serialNum;

	/**
	 * @Type("integer")
	 * */
	protected $inqControlNum;

	/**
	 * @Type("string")
	 * */
	protected $inquiryPeriod;

	/**
	 * @Type("integer")
	 * */
	protected $inqPurpose;

	/**
	 * @Type("string")
	 * */
	protected $inqPurposeText;

	/**
	 * @Type("double")
	 * */
	protected $inqAmount;

	/**
	 * @Type("string")
	 * */
	protected $currencyCode;

	/**
	 * @Type("string")
	 * */
	protected $userReference;

	/**
	 * @Type("boolean")
	 * */
	protected $freezeFlag;

	/**
	 * @Type("boolean")
	 * */
	protected $suppressFlag;

	function getSerialNum() {
		return $this->serialNum;
	}

	function getInqControlNum() {
		return $this->inqControlNum;
	}

	function getInquiryPeriod() {
		return $this->inquiryPeriod;
	}

	function getInqPurpose() {
		return $this->inqPurpose;
	}

	function getInqPurposeText() {
		return $this->inqPurposeText;
	}

	function getInqAmount() {
		return $this->inqAmount;
	}

	function getCurrencyCode() {
		return $this->currencyCode;
	}

	function getUserReference() {
		return $this->userReference;
	}

	function getFreezeFlag() {
		return $this->freezeFlag;
	}

	function getSuppressFlag() {
		return $this->suppressFlag;
	}
}
