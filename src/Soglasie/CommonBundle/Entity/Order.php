<?php

namespace Soglasie\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 */
class Order
{
    const STATUS_CREATED = 0;
    const STATUS_PAID = 1;
    const STATUS_SENT = 2;

    static private $statuses = [
        self::STATUS_CREATED => 'Создана',
        self::STATUS_PAID => 'Оплачена',
        self::STATUS_SENT => 'Получена',
    ];

    static private $titles = [
        'Покупка кредитной истории с главной формы',
        'Покупка кредитной истории с формы акции',
        'Бесплатная консультация специалиста',
        'Получить бесплатную информацию',
        'Форма обратного звонка',
        'Покупка кредитной истории по цене 450 рублей',
        'Покупка кредитной истории по цене 700 рублей',
    ];

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $amount;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \JMS\Payment\CoreBundle\Entity\PaymentInstruction
     */
    private $paymentInstruction;

    /**
     * @var \Soglasie\UserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return Order
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Order
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set paymentInstruction
     *
     * @param \JMS\Payment\CoreBundle\Entity\PaymentInstruction $paymentInstruction
     * @return Order
     */
    public function setPaymentInstruction(\JMS\Payment\CoreBundle\Entity\PaymentInstruction $paymentInstruction = null)
    {
        $this->paymentInstruction = $paymentInstruction;

        return $this;
    }

    /**
     * Get paymentInstruction
     *
     * @return \JMS\Payment\CoreBundle\Entity\PaymentInstruction 
     */
    public function getPaymentInstruction()
    {
        return $this->paymentInstruction;
    }

    /**
     * Set user
     *
     * @param \Soglasie\UserBundle\Entity\User $user
     * @return Order
     */
    public function setUser(\Soglasie\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Soglasie\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var integer
     */
    private $type;


    /**
     * Set type
     *
     * @param integer $type
     * @return Order
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get titles
     *
     * @return array
     */
    static public function getTitlesList()
    {
        return self::$titles;
    }
    
    /**
     * Get statuses
     *
     * @return array
     */
    static public function getStatusesList()
    {
        return self::$statuses;
    }

    public function __toString()
    {
        return (string) $this->id;
    }
    /**
     * @var \Soglasie\CommonBundle\Entity\OrderData
     */
    private $orderData;


    /**
     * Set orderData
     *
     * @param \Soglasie\CommonBundle\Entity\OrderData $orderData
     * @return Order
     */
    public function setOrderData(\Soglasie\CommonBundle\Entity\OrderData $orderData = null)
    {
        $this->orderData = $orderData;

        return $this;
    }

    /**
     * Get orderData
     *
     * @return \Soglasie\CommonBundle\Entity\OrderData 
     */
    public function getOrderData()
    {
        return $this->orderData;
    }
}
