<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 18.09.14
 * Time: 16:16
 */

namespace Soglasie\CommonBundle\Tests\GeoDetectionManager;

use Soglasie\CommonBundle\Manager\GeoDetectionManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GeoDetectionManagerTest extends WebTestCase
{
    static $browser, $container;
    private $checkUrl = "http://www.kody.su/check-tel";

    static public function setUpBeforeClass()
    {
        //start the symfony kernel
        $kernel = static::createKernel();
        $kernel->boot();

        //get the DI container
        self::$container = $kernel->getContainer();

        //now we can instantiate our service (if you want a fresh one for
        //each test method, do this in setUp() instead
        self::$browser = self::$container->get('buzz');
        self::$browser->getClient()->setTimeout(20);
    }

    function testGeoDetectionManager()
    {
        $manager = new GeoDetectionManager(self::$browser, $this->checkUrl);
        $ipRegion = $manager->detectByIp('8.8.8.8');
        $phoneRegion = $manager->detectByPhone('89621234567');

        $this->assertEquals('Mountain View', $ipRegion);
        $this->assertEquals('Сахалинская область', $phoneRegion);
    }
}
