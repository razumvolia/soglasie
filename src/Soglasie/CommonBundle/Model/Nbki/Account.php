<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:41
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation\Type;

class Account {
	/**
	 * @Type("integer")
	 * */
	protected $serialNum;

	/**
	 * @Type("string")
	 * */
	protected $acctNum;

	function getSerialNum() {
		return $this->serialNum;
	}

	function getAcctNum() {
		return $this->acctNum;
	}
}
