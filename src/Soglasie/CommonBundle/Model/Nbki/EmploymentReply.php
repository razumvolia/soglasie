<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:42
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class EmploymentReply {
	/**
	 * @JMS\Type("integer")
	 * */
	protected $serialNum;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("fileSinceDt")
	 * */
	protected $fileSinceDate;

	/**
	 * @JMS\Type("string")
	 * */
	protected $name;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $occupation;

	/**
	 * @JMS\Type("string")
	 * */
	protected $occupationText;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $occupationStatus;

	/**
	 * @JMS\Type("string")
	 * */
	protected $occupationStatusText;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $occupationTrade;

	/**
	 * @JMS\Type("string")
	 * */
	protected $occupationTradeText;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("startDt")
	 * */
	protected $startDate;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $current;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $title;

	/**
	 * @JMS\Type("string")
	 * */
	protected $titleText;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("lastUpdatedDt")
	 * */
	protected $lastUpdatedDate;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $freezeFlag;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $suppressFlag;

	/**
	 * @JMS\Type("string")
	 * */
	protected $disputedStatus;

	/**
	 * @JMS\Type("string")
	 * */
	protected $disputedRemarks;

	function getSerialNum() {
		return $this->serialNum;
	}

	function getFileSinceDate() {
		return $this->fileSinceDate;
	}

	function getName() {
		return $this->name;
	}

	function getOccupation() {
		return $this->occupation;
	}

	function getOccupationText() {
		return $this->occupationText;
	}

	function getOccupationStatus() {
		return $this->occupationStatus;
	}

	function getOccupationStatusText() {
		return $this->occupationStatusText;
	}

	function getOccupationTrade() {
		return $this->occupationTrade;
	}

	function getOccupationTradeText() {
		return $this->occupationTradeText;
	}

	function getStartDate() {
		return $this->startDate;
	}

	function getCurrent() {
		return $this->current;
	}

	function getTitle() {
		return $this->title;
	}

	function getTitleText() {
		return $this->titleText;
	}

	function getLastUpdatedDate() {
		return $this->lastUpdatedDate;
	}

	function getFreezeFlag() {
		return $this->freezeFlag;
	}

	function getSuppressFlag() {
		return $this->suppressFlag;
	}

	function getDisputedStatus() {
		return $this->disputedStatus;
	}

	function getDisputedRemarks() {
		return $this->disputedRemarks;
	}
}
