<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:43
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation\Type;

class Calc
{
    /**
     * Количество всех кредитов
     * @Type("integer")
     * */
    protected $totalAccts;

    /**
     * Количество активных кредитов
     * @Type("integer")
     * */
    protected $totalActiveBalanceAccounts;

    /**
     * Количество кредитов с негативным рейтингом. Например, если по кредиту была просрочка и затем его выплатили, то он
     * всё равно останется с негативным рейтингом.
     * @Type("integer")
     * */
    protected $negativeRating;

    /**
     * @Type("DateTime<'Y-m-dP'>")
     * */
    protected $mostRecentAcc;

    /**
     * @Type("DateTime<'Y-m-dP'>")
     * */
    protected $oldest;

    /**
     * @Type("integer")
     * */
    protected $totalOfficialInfo;

    /**
     * @Type("integer")
     * */
    protected $totalLegalItems;

    /**
     * @Type("integer")
     * */
    protected $totalBankruptcies;

    /**
     * @Type("DateTime<'Y-m-dP'>")
     * */
    protected $mostRecentOfficialInfo;

    /**
     * @Type("DateTime<'Y-m-dP'>")
     * */
    protected $mostRecentLegalItem;

    /**
     * Полное количество обращений за кредитами
     * @Type("integer")
     * */
    protected $totalInquiries;

    /**
     * Количество обращений за кредитами за крайние 30 дней
     * @Type("integer")
     * */
    protected $recentInquiries;

    /**
     * Количество обращений за кредитами за крайний год
     * @Type("integer")
     * */
    protected $collectionsInquiries;

    /**
     * @Type("string")
     * */
    protected $mostRecentInqText;

    /**
     * @Type("DateTime<'Y-m-d\TH:i:s.uP'>")
     * */
    protected $reportIssueDateTime;

    /**
     * @Type("integer")
     * */
    protected $totalDisputedAccounts;

    /**
     * @Type("integer")
     * */
    protected $totalDisputedBankruptcy;

    /**
     * @Type("integer")
     * */
    protected $totalDisputedLegalItem;

    /**
     * @Type("integer")
     * */
    protected $totalDisputedOfficialInfo;

    /**
     * @Type("Soglasie\CommonBundle\Model\Nbki\TotalMoney")
     * */
    protected $totalHighCredit;

    /**
     * Текущий баланс/Всего выплачено по кредитам
     * @Type("Soglasie\CommonBundle\Model\Nbki\TotalMoney")
     * */
    protected $totalCurrentBalance;

    /**
     * Просроченная задолженность по кредитам
     * @Type("Soglasie\CommonBundle\Model\Nbki\TotalMoney")
     * */
    protected $totalPastDueBalance;

    /**
     * Задолженность по кредитам
     * @Type("Soglasie\CommonBundle\Model\Nbki\TotalMoney")
     * */
    protected $totalOutstandingBalance;

    /**
     * @Type("Soglasie\CommonBundle\Model\Nbki\TotalMoney")
     * */
    protected $totalScheduledPaymnts;

    /**
     * @Type("integer")
     * */
    protected $total30Days;

    /**
     * @Type("integer")
     * */
    protected $total60Days;

    /**
     * @Type("integer")
     * */
    protected $total90Days;

    function getTotal30Days()
    {
		return $this->total30Days;
    }

    function setTotal30Days($total30Days)
    {
		$this->total30Days = $total30Days;
    }

	function getTotal60Days()
	{
		return $this->total60Days;
	}

	function setTotal60Days($total60Days)
    {
		$this->total60Days = $total60Days;
    }

	function getTotal90Days()
	{
		return $this->total90Days;
	}

	function setTotal90Days($total90Days)
    {
		$this->total90Days = $total90Days;
    }

    function getTotalAccts()
    {
        return $this->totalAccts;
    }

    function getTotalActiveBalanceAccounts()
    {
        return $this->totalActiveBalanceAccounts;
    }

    function getNegativeRating()
    {
        return $this->negativeRating;
    }

    function getMostRecentAcc()
    {
        return $this->mostRecentAcc;
    }

    function getOldest()
    {
        return $this->oldest;
    }

    function getTotalOfficialInfo()
    {
        return $this->totalOfficialInfo;
    }

    function getTotalLegalItems()
    {
        return $this->totalLegalItems;
    }

    function getTotalBankruptcies()
    {
        return $this->totalBankruptcies;
    }

    function getMostRecentOfficialInfo()
    {
        return $this->mostRecentOfficialInfo;
    }

    function getMostRecentLegalItem()
    {
        return $this->mostRecentLegalItem;
    }

    function getTotalInquiries()
    {
        return $this->totalInquiries;
    }

    function getRecentInquiries()
    {
        return $this->recentInquiries;
    }

    function getCollectionsInquiries()
    {
        return $this->collectionsInquiries;
    }

    function getMostRecentInqText()
    {
        return $this->mostRecentInqText;
    }

    function getReportIssueDateTime()
    {
        return $this->reportIssueDateTime;
    }

    function getTotalDisputedAccounts()
    {
        return $this->totalDisputedAccounts;
    }

    function getTotalDisputedBankruptcy()
    {
        return $this->totalDisputedBankruptcy;
    }

    function getTotalDisputedLegalItem()
    {
        return $this->totalDisputedLegalItem;
    }

    function getTotalDisputedOfficialInfo()
    {
        return $this->totalDisputedOfficialInfo;
    }

    function getTotalHighCredit()
    {
        return $this->totalHighCredit;
    }

    function getTotalCurrentBalance()
    {
        return $this->totalCurrentBalance;
    }

    function getTotalPastDueBalance()
    {
        return $this->totalPastDueBalance;
    }

    function getTotalOutstandingBalance()
    {
        return $this->totalOutstandingBalance;
    }

    function getTotalScheduledPaymnts()
    {
        return $this->totalScheduledPaymnts;
    }

}
