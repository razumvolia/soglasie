set :application, "soglasie"

role :app, ""

set :symfony_console, "bin/console"

set :repository, "."
set :scm, :none
set :scm_verbose, true
set :deploy_to, "/var/www/#{application}"
set :deploy_via, :copy
set :copy_remote_dir, deploy_to

set :use_sudo, false
set :use_composer, true
set :composer_bin, "/usr/bin/composer"
set :keep_releases, 3

set :dump_assetic_assets, true

set :user, 'ubuntu'
set :permission_method, :acl
set :use_set_permissions, false
set :shared_files, ["app/config/parameters.yml", "app/config/parameters_prod.yml"]
set :writable_dirs, ["var/cache"]
set :shared_children, ["var/logs", "uploads"]

after "deploy", "clear_cache"
after "deploy:update", "symfony:doctrine:schema:update"
after 'deploy:update', 'deploy:cleanup'

task :clear_cache do
  run "rm -rf #{release_path}/var/cache/*"
end
