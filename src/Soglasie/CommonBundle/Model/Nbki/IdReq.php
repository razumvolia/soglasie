<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:31
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation\Type;

class IdReq {
	/**
	 * @Type("integer")
	 * */
	protected $idType = 21;

	/**
	 * @Type("integer")
	 * */
	protected $seriesNumber = 1234;

	/**
	 * @Type("integer")
	 * */
	protected $idNum = 123456;

	/**
	 * @Type("string")
	 * */
	protected $issueCountry = 'г. Магадан';

	/**
	 * @Type("string")
	 * */
	protected $issueDate = '2013-11-11';

	/**
	 * @Type("string")
	 * */
	protected $issueAuthority = 'ОВД Центральное, к/п 255';

	function setIdSeries( $idSeries ) {
		$this->seriesNumber = $idSeries;
	}

	function setIdNumber( $idNum ) {
		$this->idNum = $idNum;
	}
}
