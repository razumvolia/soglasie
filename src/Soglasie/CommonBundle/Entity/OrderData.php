<?php

namespace Soglasie\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderData
 */
class OrderData
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $first_name;

    /**
     * @var string
     */
    private $middle_name;

    /**
     * @var string
     */
    private $last_name;

    /**
     * @var \DateTime
     */
    private $birth_date;

    /**
     * @var integer
     */
    private $passport_series;

    /**
     * @var integer
     */
    private $passport_number;

    /**
     * @var string
     */
    private $passport_issuer;

    /**
     * @var \DateTime
     */
    private $passport_issue_date;

    /**
     * @var string
     */
    private $subdivision_code;

    /**
     * @var string
     */
    private $place_of_birth;

    /**
     * @var string
     */
    private $registration_address;

    /**
     * @var string
     */
    private $actual_address;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set first_name
     *
     * @param string $firstName
     * @return OrderData
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get first_name
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set middle_name
     *
     * @param string $middleName
     * @return OrderData
     */
    public function setMiddleName($middleName)
    {
        $this->middle_name = $middleName;

        return $this;
    }

    /**
     * Get middle_name
     *
     * @return string 
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * Set last_name
     *
     * @param string $lastName
     * @return OrderData
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get last_name
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set birth_date
     *
     * @param \DateTime $birthDate
     * @return OrderData
     */
    public function setBirthDate($birthDate)
    {
        $this->birth_date = $birthDate;

        return $this;
    }

    /**
     * Get birth_date
     *
     * @return \DateTime 
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * Set passport_series
     *
     * @param integer $passportSeries
     * @return OrderData
     */
    public function setPassportSeries($passportSeries)
    {
        $this->passport_series = $passportSeries;

        return $this;
    }

    /**
     * Get passport_series
     *
     * @return integer 
     */
    public function getPassportSeries()
    {
        return $this->passport_series;
    }

    /**
     * Set passport_number
     *
     * @param integer $passportNumber
     * @return OrderData
     */
    public function setPassportNumber($passportNumber)
    {
        $this->passport_number = $passportNumber;

        return $this;
    }

    /**
     * Get passport_number
     *
     * @return integer 
     */
    public function getPassportNumber()
    {
        return $this->passport_number;
    }

    /**
     * Set passport_issuer
     *
     * @param string $passportIssuer
     * @return OrderData
     */
    public function setPassportIssuer($passportIssuer)
    {
        $this->passport_issuer = $passportIssuer;

        return $this;
    }

    /**
     * Get passport_issuer
     *
     * @return string 
     */
    public function getPassportIssuer()
    {
        return $this->passport_issuer;
    }

    /**
     * Set passport_issue_date
     *
     * @param \DateTime $passportIssueDate
     * @return OrderData
     */
    public function setPassportIssueDate($passportIssueDate)
    {
        $this->passport_issue_date = $passportIssueDate;

        return $this;
    }

    /**
     * Get passport_issue_date
     *
     * @return \DateTime 
     */
    public function getPassportIssueDate()
    {
        return $this->passport_issue_date;
    }

    /**
     * Set subdivision_code
     *
     * @param string $subdivisionCode
     * @return OrderData
     */
    public function setSubdivisionCode($subdivisionCode)
    {
        $this->subdivision_code = $subdivisionCode;

        return $this;
    }

    /**
     * Get subdivision_code
     *
     * @return string 
     */
    public function getSubdivisionCode()
    {
        return $this->subdivision_code;
    }

    /**
     * Set place_of_birth
     *
     * @param string $placeOfBirth
     * @return OrderData
     */
    public function setPlaceOfBirth($placeOfBirth)
    {
        $this->place_of_birth = $placeOfBirth;

        return $this;
    }

    /**
     * Get place_of_birth
     *
     * @return string 
     */
    public function getPlaceOfBirth()
    {
        return $this->place_of_birth;
    }

    /**
     * Set registration_address
     *
     * @param string $registrationAddress
     * @return OrderData
     */
    public function setRegistrationAddress($registrationAddress)
    {
        $this->registration_address = $registrationAddress;

        return $this;
    }

    /**
     * Get registration_address
     *
     * @return string 
     */
    public function getRegistrationAddress()
    {
        return $this->registration_address;
    }

    /**
     * Set actual_address
     *
     * @param string $actualAddress
     * @return OrderData
     */
    public function setActualAddress($actualAddress)
    {
        $this->actual_address = $actualAddress;

        return $this;
    }

    /**
     * Get actual_address
     *
     * @return string 
     */
    public function getActualAddress()
    {
        return $this->actual_address;
    }
    /**
     * @var \Soglasie\CommonBundle\Entity\Order
     */
    private $order;


    /**
     * Set order
     *
     * @param \Soglasie\CommonBundle\Entity\Order $order
     * @return OrderData
     */
    public function setOrder(\Soglasie\CommonBundle\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Soglasie\CommonBundle\Entity\Order 
     */
    public function getOrder()
    {
        return $this->order;
    }
}
