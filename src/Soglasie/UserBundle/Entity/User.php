<?php

namespace Soglasie\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser;

/**
 * User
 */
class User extends BaseUser
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @var string
     */
    private $name;


    /**
     * @var integer
     */
    private $bitrixId;


    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set bitrixId
     *
     * @param integer $bitrixId
     * @return User
     */
    public function setBitrixId($bitrixId)
    {
        $this->bitrixId = $bitrixId;

        return $this;
    }

    /**
     * Get bitrixId
     *
     * @return integer 
     */
    public function getBitrixId()
    {
        return $this->bitrixId;
    }
}
