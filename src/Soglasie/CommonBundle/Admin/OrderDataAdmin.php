<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 24.09.14
 * Time: 15:12
 */

namespace Soglasie\CommonBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class OrderDataAdmin extends Admin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clear();
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('last_name', 'text', ['label' => 'sonata.field.last_name'])
            ->add('first_name', 'text', ['label' => 'sonata.field.first_name'])
            ->add('middle_name', 'text', ['label' => 'sonata.field.middle_name'])
            ->add('birth_date', 'date', ['label' => 'sonata.field.birth_date', 'widget' => 'single_text', 'format' => 'd.M.y'])
            ->add('passport_series', 'number', ['label' => 'sonata.field.passport_series'])
            ->add('passport_number', 'number', ['label' => 'sonata.field.passport_number'])
        ;
    }

}