<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:41
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class PhoneReply {
	/**
	 * @JMS\Type("integer")
	 * */
	protected $serialNum;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $number;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("fileSinceDt")
	 * */
	protected $fileSinceDate;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $phoneType;

	/**
	 * @JMS\Type("string")
	 * */
	protected $phoneTypeText;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("lastUpdatedDt")
	 * */
	protected $lastUpdatedDate;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $freezeFlag;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $suppressFlag;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $disputedStatus;

	/**
	 * @JMS\Type("string")
	 * */
	protected $disputedRemarks;

	function getSerialNum() {
		return $this->serialNum;
	}

	function getNumber() {
		return $this->number;
	}

	function getFileSinceDate() {
		return $this->fileSinceDate;
	}

	function getPhoneType() {
		return $this->phoneType;
	}

	function getPhoneTypeText() {
		return $this->phoneTypeText;
	}

	function getLastUpdatedDate() {
		return $this->lastUpdatedDate;
	}

	function getFreezeFlag() {
		return $this->freezeFlag;
	}

	function getSuppressFlag() {
		return $this->suppressFlag;
	}

	function getDisputedStatus() {
		return $this->disputedStatus;
	}

	function getDisputedRemarks() {
		return $this->disputedRemarks;
	}
}
