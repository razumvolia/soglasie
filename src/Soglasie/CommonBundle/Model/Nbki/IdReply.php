<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 14.08.14
 * Time: 21:40
 */

namespace Soglasie\CommonBundle\Model\Nbki;

use JMS\Serializer\Annotation as JMS;

class IdReply extends IdReq {
	/**
	 * @JMS\Type("integer")
	 * */
	protected $serialNum;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("fileSinceDt")
	 * */
	protected $fileSinceDate;

	/**
	 * @JMS\Type("string")
	 * */
	protected $idTypeText;

	/**
	 * @JMS\Type("DateTime<'Y-m-dP'>")
	 * @JMS\SerializedName("lastUpdatedDt")
	 * */
	protected $lastUpdatedDate;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $freezeFlag;

	/**
	 * @JMS\Type("boolean")
	 * */
	protected $suppressFlag;

	/**
	 * @JMS\Type("integer")
	 * */
	protected $disputedStatus;

	/**
	 * @JMS\Type("string")
	 * */
	protected $disputedRemarks;

	function getSerialNum() {
		return $this->serialNum;
	}

	function getFileSinceDate() {
		return $this->fileSinceDate;
	}

	function getIdTypeText() {
		return $this->idTypeText;
	}

	function getLastUpdatedDate() {
		return $this->lastUpdatedDate;
	}

	function getFreezeFlag() {
		return $this->freezeFlag;
	}

	function getSuppressFlag() {
		return $this->suppressFlag;
	}

	function getDisputedStatus() {
		return $this->disputedStatus;
	}

	function getDisputedRemarks() {
		return $this->disputedRemarks;
	}
}
