<?php
/**
 * Created by PhpStorm.
 * User: countzero
 * Date: 10.09.14
 * Time: 11:33
 */

namespace Soglasie\CommonBundle\Manager;

use Buzz\Browser;

class GeoDetectionManager
{
    private $browser;

    function __construct(Browser $browser, $checkUrl)
    {
        $this->browser = $browser;
        $this->checkUrl = $checkUrl;
    }

    function detectByIp($ip)
    {
        $result = @geoip_record_by_name($ip);

        return $result ? $result['city'] : false;
    }

    function detectByPhone($phone)
    {
        $this->browser->getClient()->setIgnoreErrors(true);
        $content = $this->browser->post($this->checkUrl, [], ['tel' => $phone]);
        $region = preg_match('~<strong style="font-size:12px">[^<]+\[([^<]+)\], <br>~', $content, $matches) ? $matches[1] : false;
        $region = iconv('windows-1251', 'utf-8', $region);

        return $region;
    }
}
